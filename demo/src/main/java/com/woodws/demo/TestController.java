package com.woodws.demo;

import com.woodws.core.annotation.I18n;
import com.woodws.document.annotation.DocAttribute;
import com.woodws.document.annotation.Import;
import com.woodws.sys.entity.SysUser;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by maoxiaodong on 2016/10/28.
 */
@Controller
@RequestMapping("demo")
public class TestController {

    private MessageSource messageSource;

    @RequestMapping
    public String test(){
        return "/demo/home";
    }

    @Import("user.xml")
    @RequestMapping("/imp")
    @ResponseBody
    public boolean imp(@DocAttribute SysUser user) {
        return true;
    }

    @RequestMapping("I18n")
    @I18n({"paramMap.test", "user.name"})
    @ResponseBody
    public String I18n(@ModelAttribute Map paramMap, Model model) {
        paramMap.put("test", "密码");
        SysUser user = new SysUser();
        user.setName("用户名");
        model.addAttribute("user", user);
        return "";
    }

    @RequestMapping("exp")
    public ModelAndView imp() {
        SysUser user = new SysUser();
        user.setName("1111");
        List<SysUser> users = new ArrayList<SysUser>();
        users.add(user);
        ModelAndView modelAndView = new ModelAndView("/export/test.xlsx").addObject("users", users);
        return modelAndView;
    }
}
