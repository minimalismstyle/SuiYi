package com.woodws.model.entity;

import javax.persistence.*;
import java.util.List;

/**
 * Created by maoxiaodong on 2016/10/21.
 */
@Entity
public class Model {
    @Id
    private Long id;


    private String code;


    private String name;


    private String sqlId;


    private String view;


    private int selectType;


    private String dataUrl;


    private String dataSource;

    @Transient
    private ModelSql sql;

    @Transient
    private List<ModelParameter> parameters;

    @Transient
    private List<ModelColumn> columns;


    @Transient
    private List<ModelButton> buttons;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSqlId() {
        return sqlId;
    }

    public void setSqlId(String sqlId) {
        this.sqlId = sqlId;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    public int getSelectType() {
        return selectType;
    }

    public void setSelectType(int selectType) {
        this.selectType = selectType;
    }

    public String getDataUrl() {
        return dataUrl;
    }

    public void setDataUrl(String dataUrl) {
        this.dataUrl = dataUrl;
    }

    public String getDataSource() {
        return dataSource;
    }

    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    public ModelSql getSql() {
        return sql;
    }

    public void setSql(ModelSql sql) {
        this.sql = sql;
    }

    public List<ModelParameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<ModelParameter> parameters) {
        this.parameters = parameters;
    }

    public List<ModelColumn> getColumns() {
        return columns;
    }

    public void setColumns(List<ModelColumn> columns) {
        this.columns = columns;
    }

    public List<ModelButton> getButtons() {
        return buttons;
    }

    public void setButtons(List<ModelButton> buttons) {
        this.buttons = buttons;
    }
}
