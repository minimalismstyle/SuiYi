package com.woodws.model.dao;

import com.woodws.model.entity.ModelButton;
import com.woodws.core.dao.BaseDao;

import java.util.List;


public interface ModelButtonDao extends BaseDao<ModelButton, Long> {

    List<ModelButton> findByModelId(Long modelId);
}
