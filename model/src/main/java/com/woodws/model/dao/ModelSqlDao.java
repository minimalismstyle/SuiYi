package com.woodws.model.dao;

import com.woodws.core.dao.BaseDao;
import com.woodws.model.entity.ModelSql;

import java.util.List;


public interface ModelSqlDao extends BaseDao<ModelSql, Long> {
    List<ModelSql> findByName(String name);
}
