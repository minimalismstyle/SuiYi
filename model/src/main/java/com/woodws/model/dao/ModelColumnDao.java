package com.woodws.model.dao;

import com.woodws.model.entity.ModelColumn;
import com.woodws.core.dao.BaseDao;

import java.util.List;


public interface ModelColumnDao extends BaseDao<ModelColumn, Long> {

    List<ModelColumn> findByModelId(Long modelId);
}
