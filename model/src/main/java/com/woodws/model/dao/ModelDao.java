package com.woodws.model.dao;

import com.woodws.model.entity.Model;
import com.woodws.core.dao.BaseDao;


public interface ModelDao extends BaseDao<Model, Long> {

    Model findByCode(String code);
}
