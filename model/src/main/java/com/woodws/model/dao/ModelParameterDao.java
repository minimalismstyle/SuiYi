package com.woodws.model.dao;

import com.woodws.model.entity.ModelParameter;
import com.woodws.core.dao.BaseDao;

import java.util.List;


public interface ModelParameterDao extends BaseDao<ModelParameter, Long> {

    List<ModelParameter> findByModelId(Long modelId);
}
