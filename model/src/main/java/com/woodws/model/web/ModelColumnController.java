package com.woodws.model.web;

import com.woodws.core.web.WebController;
import com.woodws.model.entity.ModelColumn;
import com.woodws.model.service.ModelColumnService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/model/column/")
public class ModelColumnController extends WebController<ModelColumnService, ModelColumn, Long> {

}
