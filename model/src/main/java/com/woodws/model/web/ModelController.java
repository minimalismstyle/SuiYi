package com.woodws.model.web;

import com.woodws.core.entity.QueryParams;
import com.woodws.core.web.WebController;
import com.woodws.model.entity.Model;
import com.woodws.model.service.ModelService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

/**
 * Created by maoxiaodong on 2016/10/21.
 */
@Controller
@RequestMapping("/model/")
public class ModelController extends WebController<ModelService, Model, Long> {

    @RequestMapping("open/{code}")
    public ModelAndView open(@PathVariable("code") String code,
                             Map<String, Long> params) {
        Model entity = service.findByCode(code);
        String dataUrl = entity.getDataUrl();
        if (entity.getDataSource().equals("SQL")) {
            dataUrl = "/model/findData/" + entity.getId();
        }
        return new ModelAndView(entity.getView())
                .addObject("entity", entity)
                .addObject("dataUrl", dataUrl);
    }

    @RequestMapping("findData/{id}")
    @ResponseBody
    public List<Map<String, Object>> findData(@PathVariable("id") Long id,
                                              QueryParams params) {
        return service.getData(id, params);
    }
}
