package com.woodws.model.web;

import com.woodws.core.web.WebController;
import com.woodws.model.entity.ModelButton;
import com.woodws.model.service.ModelButtonService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/model/button/")
public class ModelButtonController extends WebController<ModelButtonService, ModelButton, Long> {

}
