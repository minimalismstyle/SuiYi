package com.woodws.model.web;

import com.woodws.core.web.WebController;
import com.woodws.model.entity.ModelSql;
import com.woodws.model.service.ModelSqlService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/model/sql/")
public class ModelSqlController extends WebController<ModelSqlService, ModelSql, Long> {

}
