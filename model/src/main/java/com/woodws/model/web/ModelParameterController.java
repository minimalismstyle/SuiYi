package com.woodws.model.web;

import com.woodws.core.web.WebController;
import com.woodws.model.entity.ModelParameter;
import com.woodws.model.service.ModelParameterService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/model/parameter/")
public class ModelParameterController extends WebController<ModelParameterService, ModelParameter, Long> {

}
