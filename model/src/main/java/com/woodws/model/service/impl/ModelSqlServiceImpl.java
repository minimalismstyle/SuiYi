package com.woodws.model.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.woodws.core.entity.PageArrayList;
import com.woodws.core.entity.PageList;
import com.woodws.core.entity.QueryParams;
import com.woodws.core.service.impl.BaseServiceImpl;
import com.woodws.model.dao.ModelSqlDao;
import com.woodws.model.entity.ModelSql;
import com.woodws.model.service.ModelSqlService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by maoxiaodong on 2016/10/16.
 */
@Service
public class ModelSqlServiceImpl extends BaseServiceImpl<ModelSqlDao, ModelSql, Long>
        implements ModelSqlService {

    @Override
    public List<ModelSql> findByParamsPage(QueryParams params) {
        Page page = PageHelper.startPage(params.getPage(), params.getSize());
        List<ModelSql> list = dao.findAll();
        PageList<ModelSql> pageList = new PageArrayList<ModelSql>(params.getPage(), (int) page.getTotal(),list);
        return pageList;
    }
}

