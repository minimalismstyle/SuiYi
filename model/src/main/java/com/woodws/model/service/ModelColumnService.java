package com.woodws.model.service;

import com.woodws.core.service.BaseService;
import com.woodws.model.entity.ModelColumn;

import java.util.List;

/**
 * Created by maoxiaodong on 2016/10/16.
 */
public interface ModelColumnService extends BaseService<ModelColumn, Long> {
    List<ModelColumn> findByModelId(Long modelId);
}
