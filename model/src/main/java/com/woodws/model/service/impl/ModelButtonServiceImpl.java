package com.woodws.model.service.impl;

import com.woodws.core.service.impl.BaseServiceImpl;
import com.woodws.model.dao.ModelButtonDao;
import com.woodws.model.entity.ModelButton;
import com.woodws.model.service.ModelButtonService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by maoxiaodong on 2016/10/16.
 */
@Service
public class ModelButtonServiceImpl extends BaseServiceImpl<ModelButtonDao, ModelButton, Long>
        implements ModelButtonService {

    @Override
    public List<ModelButton> findBy(ModelButton entity) {
        return dao.findByModelId(3L);
    }

    @Override
    public List<ModelButton> findByModelId(Long modelId) {
        return dao.findByModelId(modelId);
    }
}

