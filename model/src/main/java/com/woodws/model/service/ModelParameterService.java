package com.woodws.model.service;

import com.woodws.core.service.BaseService;
import com.woodws.model.entity.ModelParameter;

import java.util.List;

/**
 * Created by maoxiaodong on 2016/10/16.
 */
public interface ModelParameterService extends BaseService<ModelParameter, Long> {
    List<ModelParameter> findByModelId(Long modelId);
}
