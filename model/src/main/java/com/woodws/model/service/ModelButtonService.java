package com.woodws.model.service;

import com.woodws.core.service.BaseService;
import com.woodws.model.entity.ModelButton;

import java.util.List;

/**
 * Created by maoxiaodong on 2016/10/16.
 */
public interface ModelButtonService extends BaseService<ModelButton, Long> {
    List<ModelButton> findByModelId(Long modelId);
}
