package com.woodws.model.service.impl;

import com.github.pagehelper.PageHelper;
import com.woodws.core.entity.QueryParams;
import com.woodws.core.service.impl.BaseServiceImpl;
import com.woodws.model.dao.ModelDao;
import com.woodws.model.entity.Model;
import com.woodws.model.entity.ModelSql;
import com.woodws.model.service.ModelButtonService;
import com.woodws.model.service.ModelColumnService;
import com.woodws.model.service.ModelParameterService;
import com.woodws.model.service.ModelService;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by maoxiaodong on 2016/10/21.
 */
@Service
public class ModelServiceImpl extends BaseServiceImpl<ModelDao, Model, Long> implements ModelService {

    @Autowired
    private SqlSessionFactory sqlSessionFactory;

    @Autowired
    private ModelColumnService columnService;

    @Autowired
    private ModelParameterService parameterService;

    @Autowired
    private ModelButtonService buttonService;

    @Override
    public Model findByCode(String code) {
        Model model = dao.findByCode(code);
        model.setColumns(columnService.findByModelId(model.getId()));
        model.setButtons(buttonService.findByModelId(model.getId()));
        model.setParameters(parameterService.findByModelId(model.getId()));
        return model;
    }

    @Override
    public List<Map<String, Object>> getData(Long id, QueryParams params) {
        Model model = dao.findById(id).get();
        SqlSession sqlSession = sqlSessionFactory.openSession();
        if(params.isPage()){
            PageHelper.startPage(params.getPage(), params.getSize());
        }
        params.set("searchKey",params.getSearchKey());
        ModelSql modelSql = model.getSql();
        List<Map<String, Object>> data = null;
        sqlSession.close();
        return data;
    }
}
