package com.woodws.model.service.impl;

import com.woodws.core.service.impl.BaseServiceImpl;
import com.woodws.model.dao.ModelParameterDao;
import com.woodws.model.entity.ModelParameter;
import com.woodws.model.service.ModelParameterService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by maoxiaodong on 2016/10/16.
 */
@Service
public class ModelParameterServiceImpl extends BaseServiceImpl<ModelParameterDao, ModelParameter, Long>
        implements ModelParameterService {

    @Override
    public List<ModelParameter> findBy(ModelParameter entity) {
        return dao.findByModelId(entity.getModelId());
    }

    @Override
    public List<ModelParameter> findByModelId(Long modelId) {
        return dao.findByModelId(modelId);
    }
}

