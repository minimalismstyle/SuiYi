package com.woodws.model.service;

import com.woodws.core.entity.QueryParams;
import com.woodws.core.service.BaseService;
import com.woodws.model.entity.Model;

import java.util.List;
import java.util.Map;

/**
 * Created by maoxiaodong on 2016/10/21.
 */
public interface ModelService extends BaseService<Model, Long> {
    Model findByCode(String code);

    List<Map<String,Object>> getData(Long id,QueryParams params);
}
