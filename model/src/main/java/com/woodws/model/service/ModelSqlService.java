package com.woodws.model.service;

import com.woodws.core.service.BaseService;
import com.woodws.model.entity.ModelSql;

/**
 * Created by maoxiaodong on 2016/10/16.
 */
public interface ModelSqlService extends BaseService<ModelSql, Long> {
}
