package com.woodws.model.service.impl;

import com.woodws.core.service.impl.BaseServiceImpl;
import com.woodws.model.dao.ModelColumnDao;
import com.woodws.model.entity.ModelColumn;
import com.woodws.model.service.ModelColumnService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by maoxiaodong on 2016/10/16.
 */
@Service
public class ModelColumnServiceImpl extends BaseServiceImpl<ModelColumnDao, ModelColumn, Long>
        implements ModelColumnService {

    @Override
    public List<ModelColumn> findBy(ModelColumn entity) {
        return dao.findByModelId(entity.getModelId());
    }

    @Override
    public List<ModelColumn> findByModelId(Long modelId) {
        return dao.findByModelId(modelId);
    }
}

