<#include "/layout/system.ftl">
<@head>
<title>SQL定义维护</title>
<script src="${ctx}/js/core/checkboxUtil.js"></script>
<script src="${ctx}/js/core/selectUtil.js"></script>
<script src="${ctx}/js/artTemplate/template.js"></script>
<link href="${ctx}/css/toastr/toastr.min.css" rel="stylesheet">
<script src="${ctx}/js/toastr/toastr.min.js"></script>
</@head>
<@body>
<div class="wrapper wrapper-content">
    <div class="row">
        <@form.form action="${ctx}/lookup/save" method="post" class="form-horizontal" modelAttribute="entity">

            <div class="col-sm-12 animated fadeInRight">
                <div class="mail-box-header">
                    <div class="pull-right tooltip-demo">
                        <a href="javascript:save()" class="btn btn-white btn-sm"
                           data-toggle="tooltip" data-placement="top" title="保存">
                            <i class="fa fa-save"></i> 保存
                        </a>
                        <a href="${ctx}/lookup/open/lookup_sql_list" class="btn btn-danger btn-sm"
                           data-toggle="tooltip" data-placement="top" title="返回">
                            <i class="fa fa-mail-reply"></i> 返回
                        </a>
                    </div>
                    <h2>编辑LOOKUP信息</h2>
                </div>

                <div class="ibox-content">
                    <@form.hidden path="id"/>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">代码：</label>
                        <div class="col-sm-4">
                            <@form.input path="code" class="form-control" readOnly="true"/>
                        </div>
                        <label class="col-sm-2 control-label">名称：</label>
                        <div class="col-sm-4">
                            <@form.input path="name" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">数据来源：</label>
                        <div class="col-sm-4">
                            <@form.select path="dataSource" class="form-control">
                                <@form.option value="URL">URL</@form.option>
                                <@form.option value="SQL">SQL</@form.option>
                            </@form.select>
                        </div>
                        <div id="sqlEditUI">
                            <label class="col-sm-2 control-label">SQL：</label>
                            <div class="col-sm-4">
                                <@form.input path="sqlId" class="form-control"/>
                            </div>
                        </div>
                        <div id="urlEditUI" style="display: none">
                            <label class="col-sm-2 control-label">URL：</label>
                            <div class="col-sm-4">
                                <@form.input path="dataUrl" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">选择模式：</label>
                        <div class="col-sm-4">
                            <@form.select path="selectType" class="form-control">
                                <@form.option value="0">无</@form.option>
                                <@form.option value="1">单选</@form.option>
                                <@form.option value="2">多选</@form.option>
                            </@form.select>
                        </div>
                        <label class="col-sm-2 control-label">视图模式：</label>
                        <div class="col-sm-4">
                            <@form.input path="view" class="form-control"/>
                        </div>
                    </div>

                </div>

            </div>
            <div class="col-sm-12 animated fadeInRight">
                <div class="mail-box-header">
                    <div class="pull-right tooltip-demo">
                        <a href="javascript:add_columns()" class="btn btn-white btn-sm"
                           data-toggle="tooltip" data-placement="top" title="新增">
                            <i class="fa fa-save"></i> 新增
                        </a>
                        <a href="${ctx}/lookup/open/lookup_sql_list" class="btn btn-danger btn-sm"
                           data-toggle="tooltip" data-placement="top" title="删除">
                            <i class="fa fa-mail-reply"></i> 删除
                        </a>
                    </div>
                    <h4>编辑列信息</h4>
                </div>
                <div class="ibox-content">
                    <table id="columnsTable" class="table">
                        <thead>
                        <tr>
                            <th>
                                <input name="checkAll" type="checkbox">
                            </th>
                            <th class="col-sm-2">字段</th>
                            <th class="col-sm-2">名称</th>
                            <th class="col-sm-2">宽度</th>
                            <th class="col-sm-3">格式</th>
                            <th class="col-sm-1">位置</th>
                            <th class="col-sm-1">显示</th>
                            <th class="col-sm-1">排序</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="col-sm-12 animated fadeInRight">
                <div class="mail-box-header">
                    <div class="pull-right tooltip-demo">
                        <a href="javascript:add_parameters()" class="btn btn-white btn-sm"
                           data-toggle="tooltip" data-placement="top" title="新增">
                            <i class="fa fa-save"></i> 新增
                        </a>
                        <a href="${ctx}/lookup/open/lookup_sql_list" class="btn btn-danger btn-sm"
                           data-toggle="tooltip" data-placement="top" title="删除">
                            <i class="fa fa-mail-reply"></i> 删除
                        </a>
                    </div>
                    <h4>编辑参数信息</h4>
                </div>
                <div class="ibox-content">
                    <table id="parametersTable" class="table">
                        <thead>
                        <tr>
                            <th>
                                <input name="checkAll" type="checkbox">
                            </th>
                            <th class="col-sm-2">名称</th>
                            <th class="col-sm-3">标签</th>
                            <th class="col-sm-3">类型</th>
                            <th class="col-sm-3">值</th>
                            <th class="col-sm-1">排序</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="col-sm-12 animated fadeInRight">
                <div class="mail-box-header">
                    <div class="pull-right tooltip-demo">
                        <a href="javascript:add_buttons()" class="btn btn-white btn-sm"
                           data-toggle="tooltip" data-placement="top" title="新增">
                            <i class="fa fa-save"></i> 新增
                        </a>
                        <a href="${ctx}/lookup/open/lookup_sql_list" class="btn btn-danger btn-sm"
                           data-toggle="tooltip" data-placement="top" title="删除">
                            <i class="fa fa-mail-reply"></i> 删除
                        </a>
                    </div>
                    <h4>编辑按钮信息</h4>
                </div>
                <div class="ibox-content">
                    <table id="buttonsTable" class="table">
                        <thead>
                        <tr>
                            <th>
                                <input name="checkAll" type="checkbox">
                            </th>
                            <th class="col-sm-2">名称</th>
                            <th class="col-sm-2">图标</th>
                            <th class="col-sm-1">事件类型</th>
                            <th class="col-sm-4">href</th>
                            <th class="col-sm-2">位置</th>
                            <th class="col-sm-1">排序</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

            </div>
        </@form.form>
    </div>
</div>
</div>
<script id="columnsTemp" type="text/html">
    <tr>
        <td>
            <input name="id" type="checkbox" value="{{id}}">
            <input name="columns[{{index}}].id" type="hidden" value="{{id}}">
            <input name="columns[{{index}}].lookupId" type="hidden" value="{{lookupId}}">
        </td>
        <td>
            <input type="text" name="columns[{{index}}].ref" value="{{ref}}" class="form-control">
        </td>
        <td>
            <input type="text" name="columns[{{index}}].name" value="{{name}}" class="form-control">
        </td>
        <td>
            <input type="text" name="columns[{{index}}].width" value="{{width}}" class="form-control">
        </td>
        <td>
            <input type="text" name="columns[{{index}}].formatter" value="{{formatter}}"
                   class="form-control">
        </td>
        <td>
            <select name="columns[{{index}}].align" class="form-control" def="{{align}}">
                <option value="center">中</option>
                <option value="left">左</option>
                <option value="right">右</option>
            </select>
        </td>

        <td>
            <select name="columns[{{index}}].hide" class="form-control" def="{{hide}}">
                <option value="Y">是</option>
                <option value="N">否</option>
            </select>
        </td>
        <td>
            <input type="text" name="columns[{{index}}].sort" value="{{sort}}" class="form-control">
        </td>
    </tr>
</script>
<script id="parametersTemp" type="text/html">
    <tr>
        <td>
            <input name="id" type="checkbox" value="{{id}}">
            <input name="parameters[{{index}}].lookupId" type="hidden" value="{{lookupId}}">
            <input name="parameters[{{index}}].id" type="hidden" value="{{id}}">
        </td>
        <td>
            <input type="text" name="parameters[{{index}}].name" value="{{name}}" class="form-control">
        </td>
        <td>
            <input type="text" name="parameters[{{index}}].label" value="{{label}}" class="form-control">
        </td>
        <td>
            <select name="parameters[{{index}}].type" class="form-control" def="{{type}}">
                <option value="text">文本</option>
            </select>
        </td>
        <td>
            <input type="text" name="parameters[{{index}}].value" value="{{value}}"
                   class="form-control">
        </td>
        <td>
            <input type="text" name="parameters[{{index}}].sort" value="{{sort}}" class="form-control">
        </td>

    </tr>
</script>
<script id="buttonsTemp" type="text/html">
    <tr>
        <td>
            <input name="id" type="checkbox" value="{{id}}">
            <input name="buttons[{{index}}].id" type="hidden" value="{{id}}">
        </td>
        <td>
            <input type="text" name="buttons[{{index}}].name" value="{{name}}" class="form-control">
        </td>
        <td>
            <input type="text" name="buttons[{{index}}].icon" value="{{icon}}" class="form-control">
        </td>
        <td>
            <select name="buttons[{{index}}].eventType" class="form-control" def="{{eventType}}">
                <option value="url">url</option>
            </select>
        </td>

        <td>
            <input type="text" name="buttons[{{index}}].href" value="{{href}}"
                   class="form-control">
        </td>
        <td>
            <select name="buttons[{{index}}].location" class="form-control" def="{{location}}">
                <option value="tool">工具栏</option>
                <option value="line">行</option>
            </select>
        </td>
        <td>
            <input type="text" name="buttons[{{index}}].sort" value="{{sort}}" class="form-control">
        </td>
    </tr>
</script>
<script>
    $(function () {
        dataSourceSelect();
        $("#dataSource").change(dataSourceSelect);
        var column_url = "${ctx}/model/column/findBy";
        var parameter_url = "${ctx}/model/parameter/findBy";
        var button_url = "${ctx}/model/button/findBy";
        var _csrf = $("[name=_csrf]").val();
        var params = {lookupId: $("#id").val(), _csrf: _csrf};
        $.post(column_url, params, function (data) {
            for (var i = 0; i < data.length; i++) {
                add_columns(data[i]);
            }
            $("#columnsTable").find("select").selectUtil("setValue");
        });

        $.post(parameter_url, params, function (data) {
            for (var i = 0; i < data.length; i++) {
                add_parameters(data[i]);
            }
            $("#parametersTable").find("select").selectUtil("setValue");
        });

        $.post(button_url, params, function (data) {
            for (var i = 0; i < data.length; i++) {
                add_buttons(data[i]);
            }
            $("#buttonsTable").find("select").selectUtil("setValue");
        });
    });

    function dataSourceSelect() {
        var dataSource = $("#dataSource").val();
        if (dataSource == "URL") {
            $("#urlEditUI").show();
            $("#sqlEditUI").hide();
        } else {
            $("#urlEditUI").hide();
            $("#sqlEditUI").show();
        }
    }

    function save() {
        $("#entity").ajaxSubmit({
            success: function () {
                toastr.success("保存成功！");
            },
            error: function () {
                toastr.error("保存失败！");
            }
        });

    }

    function add_columns(data) {
        var index = $("#columnsTable tbody tr").size();
        var data = $.extend(data, {index: index, lookupId: $("#id").val()});
        if (!data["sort"]) {
            data["sort"] = index;
        }
        var operateHtml = template('columnsTemp', data);
        $("#columnsTable tbody").append(operateHtml);

    }

    function add_parameters(data) {
        var index = $("#parametersTable tbody tr").size();
        var data = $.extend(data, {index: index, lookupId: $("#id").val()});
        if (!data["sort"]) {
            data["sort"] = index;
        }
        var operateHtml = template('parametersTemp', data);
        $("#parametersTable tbody").append(operateHtml);

    }

    function add_buttons(data) {
        var index = $("#buttonsTable tbody tr").size();
        var data = $.extend(data, {index: index, lookupId: $("#id").val()});
        if (!data["sort"]) {
            data["sort"] = index;
        }
        var operateHtml = template('buttonsTemp', data);
        $("#buttonsTable tbody").append(operateHtml);
    }
</script>
</@body>