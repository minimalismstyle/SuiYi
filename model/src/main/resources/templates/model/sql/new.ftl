<#include "/layout/system.ftl">
<@head>
<title>SQL定义维护</title>
<link href="${ctx}/css/codemirror/codemirror.css" rel="stylesheet">
<link href="${ctx}/css/codemirror/hint/show-hint.css" rel="stylesheet">
<script src="${ctx}/js/codemirror/codemirror.js"></script>
<script src="${ctx}/js/codemirror/mode/sql.js"></script>
<link href="${ctx}/css/toastr/toastr.min.css" rel="stylesheet">
<script src="${ctx}/js/toastr/toastr.min.js"></script>
</@head>
<@body>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-sm-12 animated fadeInRight">
            <div class="mail-box-header">
                <div class="pull-right tooltip-demo">
                    <a href="javascript:save()" class="btn btn-white btn-sm"
                       data-toggle="tooltip" data-placement="top" title="存为草稿">
                        <i class="fa fa-save"></i> 保存
                    </a>
                    <a href="${ctx}/lookup/open/lookup_sql_list" class="btn btn-danger btn-sm"
                       data-toggle="tooltip" data-placement="top" title="放弃">
                        <i class="fa fa-mail-reply"></i> 返回
                    </a>
                </div>
                <h2>编辑用户信息</h2>
            </div>
            <div class="ibox-content">
                <@form.form action="${ctx}/lookup/sql/save" method="post" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">名称：</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">SQL：</label>
                        <div class="col-sm-10">
                            <textarea id="content" name="content" class="form-control"></textarea>
                        </div>
                    </div>
                </@form.form>
            </div>

        </div>
    </div>
</div>
</div>
<script>
    window.onload = function () {
        var mime = 'text/x-mariadb';
        // get mime type
        if (window.location.href.indexOf('mime=') > -1) {
            mime = window.location.href.substr(window.location.href.indexOf('mime=') + 5);
        }
        window.editor = CodeMirror.fromTextArea(document.getElementById('content'), {
            mode: mime,
            indentWithTabs: true,
            smartIndent: true,
            lineNumbers: true,
            matchBrackets: true,
            autofocus: true,
            extraKeys: {"Ctrl-Space": "autocomplete"},
            hintOptions: {
                tables: {
                    users: {name: null, score: null, birthDate: null},
                    countries: {name: null, population: null, size: null}
                }
            }
        });
    };

    function save() {
        $("#entity").ajaxSubmit({
            resetForm: true,
            success: function () {
                toastr.success("保存成功！");
            },
            error: function () {
                toastr.error("保存失败！");
            }
        });

    }

</script>
</@body>