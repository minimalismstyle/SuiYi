<div class="modal fade " id="${entity.code}_lookupModal"
     tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="col-sm-12 animated fadeInRight">
            <div id="toolbar" class="mail-box-header">
                <form id="lookupForm" method="post" class="pull-right mail-search">
                    <div class="input-group">
                        <input type="text" class="form-control input-sm" name="searchKey"
                               placeholder="关键字">
                        <div class="input-group-btn">
                            <button id="lookupSubmit" type="button" class="btn btn-sm btn-primary">
                                搜索
                            </button>
                        </div>

                    </div>
                </form>
                <h2>${entity.name}</h2>
                <div class="mail-tools m-t-md">
                    <a name="confirm" class="btn btn-white btn-sm" data-toggle="tooltip"
                       data-placement="left" title="确定">
                        <i class="fa fa-check"></i> 确定
                    </a>
                    <a class="btn btn-white btn-sm" data-toggle="tooltip"
                       data-placement="left" title="取消" data-dismiss="modal">
                        <i class="fa fa-close"></i>
                    </a>
                </div>
            </div>
            <div class="mail-box">
                <table id="${entity.code}_lookupTable">
                    <thead>
                    <tr>
                        <th data-field="state" data-checkbox="true"></th>
                    <#list entity.columns as cloumn>
                        <th data-field="${cloumn.ref}" data-formatter="${cloumn.formatter!}">${cloumn.name}</th>
                    </#list>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $("#${entity.code}_lookupTable").bootstrapTable({
        singleSelect: lookup_options.single,
        url: "${ctx}${dataUrl}",
        height: 400,
        pagination: true
    });

    $("#${entity.code}_lookupModal").modal("show");
    $("#${entity.code}_lookupModal").find("[name=confirm]").click(function () {
        var rows = $("#${entity.code}_lookupTable").bootstrapTable("getAllSelections");
        lookup_options.callback(rows);
    });
</script>