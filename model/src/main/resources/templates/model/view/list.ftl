<#include "/layout/system.ftl">
<@head>
<title>model</title>
</@head>
<@body>
<script id="operateHtml" type="text/html">
        <#list entity.buttons as button>
            <#if button.location == "line">
                <#if button.eventType == "url">
                <a class="fa ${button.icon}" href="${ctx}${button.href}" title="${button.name}"></a>
                </#if>
            </#if>
        </#list>
</script>
<script>
    $('#lookupSubmit').click(function () {
        $("#lookupTable").bootstrapTable('refresh');

    });

    function getQueryParams(params) {
        var searchKey = $("#lookupForm").find("[name=searchKey]").val();
        $(params).attr("searchKey", searchKey);
        return params;
    }

    function operateHtml(value, row, index) {
        var operateHtml = template('operateHtml', row)
        return operateHtml;
    }
</script>
</@body>