<#macro head>
    <#assign layout_head>
        <#nested />
    </#assign>
</#macro>
<#macro body>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link href="${ctx}/lib/layui/css/layui.css" rel="stylesheet">
    <link href="${ctx}/css/custom.css" rel="stylesheet">


${layout_head}
</head>
<body class="layui-layout-body">

</body>
<script src="${ctx}/lib/layui/layui.js"></script>
<script>
    layui.use(['element', 'form', 'layedit', 'laydate','table'], function () {
        var $ = layui.jquery
                , element = layui.element; //Tab的切换功能，切换事件监听等，需要依赖element模块
        
        //触发事件
        var active = {
            tabAdd: function () {
                //新增一个Tab项
                element.tabAdd('demo', {
                    title: '新选项' + (Math.random() * 1000 | 0) //用于演示
                    , content: '内容' + (Math.random() * 1000 | 0)
                    , id: new Date().getTime() //实际使用一般是规定好的id，这里以时间戳模拟下
                })
            }
        };

    });
</script>
</html>
</#macro>