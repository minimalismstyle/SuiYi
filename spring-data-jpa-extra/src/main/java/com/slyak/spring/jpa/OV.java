package com.slyak.spring.jpa;

public interface OV {
    void set(String name,Object value);
    <T> T get(String name);
}
