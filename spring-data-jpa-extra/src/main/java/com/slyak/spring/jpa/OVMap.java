package com.slyak.spring.jpa;

import com.slyak.util.StringUtil;

import java.math.BigInteger;
import java.util.HashMap;

public class OVMap extends HashMap<String,Object> implements OV {

    static final float DEFAULT_LOAD_FACTOR = 0.75f;

    public OVMap(int initialCapacity) {
        super(initialCapacity, DEFAULT_LOAD_FACTOR);
    }

    /**
     * Constructs an empty <tt>HashMap</tt> with the default initial capacity
     * (16) and the default load factor (0.75).
     */
    public OVMap() {
        super();
    }

    @Override
    public Object put(String key, Object value) {
        if(value != null){
            if(value instanceof BigInteger){
                value = ((BigInteger) value).longValue();
            }
        }

        key = StringUtil.lineToHump(key);
        return super.put(key, value);
    }

    @Override
    public void set(String name, Object value) {
        super.put(name, value);
    }

    @Override
    public <T> T get(String name) {
        return (T)super.get(name);
    }
}
