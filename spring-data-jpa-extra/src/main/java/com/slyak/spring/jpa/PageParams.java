package com.slyak.spring.jpa;

import org.springframework.data.domain.Pageable;

import java.io.Serializable;
import java.util.Map;

public interface PageParams extends Pageable, OV, Serializable {
    boolean isPaging();
    void addParams(OV ov);
    void addParams(Map map);
}
