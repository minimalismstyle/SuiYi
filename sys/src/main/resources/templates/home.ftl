<#include "/layout/common.ftl">
<@head>
<title>home</title>
</@head>
<@body>
<div class="layui-side layui-bg-black">
    <div class="layui-side-scroll">
        <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
        <ul class="layui-nav layui-nav-tree" lay-filter="test">
            <li class="layui-nav-item layui-nav-itemed">
                <a class="" href="javascript:;">所有商品</a>
                <dl class="layui-nav-child">
                    <dd><a href="javascript:;" data-type="tabAdd">列表一</a></dd>
                    <dd><a href="javascript:;">列表二</a></dd>
                    <dd><a href="javascript:;">列表三</a></dd>
                    <dd><a href="">超链接</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item">
                <a href="javascript:;">解决方案</a>
                <dl class="layui-nav-child">
                    <dd><a href="javascript:;">列表一</a></dd>
                    <dd><a href="javascript:;">列表二</a></dd>
                    <dd><a href="">超链接</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item"><a href="">云市场</a></li>
            <li class="layui-nav-item"><a href="">发布商品</a></li>
        </ul>
    </div>
</div>

<div class="layui-body">
    <div class="layui-tab layui-tab-brief" lay-filter="demo" lay-allowclose="true">
        <ul class="layui-tab-title">
            <li class="layui-this">网站设置</li>
        </ul>
        <div class="layui-tab-content">
            <div class="layui-tab-item layui-show">
                <iframe scrolling="no" allowtransparency="true" id="layui-layer-iframe2" onload="iframe_onload();" class="" frameborder="0" src="http://layer.layui.com/" style="height: 557px;"></iframe>
                <div class="layui-form-item">
                    <div class="layui-btn-group ">
                        <button class="layui-btn layui-btn-small">
                            <i class="layui-icon">&#xe654;</i>
                        </button>
                        <button class="layui-btn layui-btn-small">
                            <i class="layui-icon">&#xe642;</i>
                        </button>
                        <button class="layui-btn layui-btn-small">
                            <i class="layui-icon">&#xe640;</i>
                        </button>
                        <button class="layui-btn layui-btn-small">
                            <i class="layui-icon">&#xe602;</i>
                        </button>
                    </div>
                    <div class="layui-inline">
                        <label class="layui-form-label">多规则验证</label>
                        <div class="layui-input-inline">
                            <input type="text" name="number" lay-verify="required|number" autocomplete="off" class="layui-input">
                        </div>
                    </div>

                </div>

                <table class="layui-table" lay-data="{height:100%, url:'http://www.layui.com/demo/table/user/'}">
                    <thead>
                    <tr>
                        <th lay-data="{field:'id', width:80, sort: true}">ID</th>
                        <th lay-data="{field:'username', width:80}">用户名</th>
                        <th lay-data="{field:'sex', width:80, sort: true}">性别</th>
                        <th lay-data="{field:'city', width:80}">城市</th>
                        <th lay-data="{field:'sign', width:177}">签名</th>
                        <th lay-data="{field:'experience', width:80, sort: true}">积分</th>
                        <th lay-data="{field:'score', width:80, sort: true}">评分</th>
                        <th lay-data="{field:'classify', width:80}">职业</th>
                        <th lay-data="{field:'wealth', width:135, sort: true}">财富</th>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="layui-tab-item">
                <iframe src="${ctx}/login"></iframe>
            </div>
            <div class="layui-tab-item">内容3</div>
            <div class="layui-tab-item">内容4</div>
            <div class="layui-tab-item">内容5</div>
        </div>
    </div>
</div>
</@body>