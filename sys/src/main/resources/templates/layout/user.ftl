<#assign form=JspTaglibs["/META-INF/spring-form.tld"] />
<#assign security=JspTaglibs["/META-INF/security.tld"] />
<@security.authentication property="principal.user.name" var="username"/>
<@security.authentication property="principal.attr['mainMenus']" var="mainMenus"/>
<#macro head>
    <#assign layout_head>
        <#nested />
    </#assign>
</#macro>
<#macro body>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link href="${ctx}/lib/layui/css/layui.css" rel="stylesheet">
    <link href="${ctx}/lib/layui/css/global.css" rel="stylesheet">


${layout_head}
</head>
<body>
<div class="header">
    <div class="main">
        <a class="logo" href="/" title="Fly">Fly社区</a>
        <div class="nav">
            <#list mainMenus as menu>
                <a class="nav-this" href="${ctx}${menu.url}">
                    <i class="iconfont icon-wenda"></i>${menu.name}
                </a>
            </#list>
        </div>

        <div class="nav-user">
            <!-- 未登入状态 -->
            <a class="unlogin" href="user/login.html"><i class="iconfont icon-touxiang"></i></a>
            <span><a href="user/login.html">登入</a><a href="user/reg.html">注册</a></span>
            <p class="out-login">
                <a href="" onclick="layer.msg('正在通过QQ登入', {icon:16, shade: 0.1, time:0})" class="iconfont icon-qq" title="QQ登入"></a>
                <a href="" onclick="layer.msg('正在通过微博登入', {icon:16, shade: 0.1, time:0})" class="iconfont icon-weibo" title="微博登入"></a>
            </p>

            <!-- 登入后的状态 -->
            <!--
            <a class="avatar" href="user/index.html">
              <img src="http://tp4.sinaimg.cn/1345566427/180/5730976522/0">
              <cite>贤心</cite>
              <i>VIP2</i>
            </a>
            <div class="nav">
              <a href="/user/set/"><i class="iconfont icon-shezhi"></i>设置</a>
              <a href="/user/logout/"><i class="iconfont icon-tuichu" style="top: 0; font-size: 22px;"></i>退了</a>
            </div> -->

        </div>
    </div>
</div>
</body>
<script src="${ctx}/lib/layui/layui.js"></script>
</html>
</#macro>