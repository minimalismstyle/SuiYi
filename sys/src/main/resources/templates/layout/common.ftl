<#assign form=JspTaglibs["/META-INF/spring-form.tld"] />
<#assign security=JspTaglibs["/META-INF/security.tld"] />
<@security.authentication property="principal.user.name" var="username"/>
<@security.authentication property="principal.attr['mainMenus']" var="mainMenus"/>
<#macro head>
    <#assign layout_head>
        <#nested />
    </#assign>
</#macro>
<#macro body>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link href="${ctx}/lib/layui/css/layui.css" rel="stylesheet">
    <link href="${ctx}/css/custom.css" rel="stylesheet">


${layout_head}
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <div class="layui-logo">layui 后台布局</div>
        <!-- 头部区域（可配合layui已有的水平导航） -->
        <ul class="layui-nav layui-layout-right">
            <#list mainMenus as menu>
                <li class="layui-nav-item">
                    <a href="${ctx}${menu.url}">${menu.name}</a>
                </li>
                <li class="layui-nav-item">
                    <a href="${ctx}${menu.url}">${menu.name}</a>
                </li>
            </#list>
            <li class="layui-nav-item">
                <a href="javascript:;">
                    <img src="http://t.cn/RCzsdCq" class="layui-nav-img">
                    贤心
                </a>
                <dl class="layui-nav-child">
                    <dd><a href="${ctx}/logout">退出</a></dd>
                </dl>
            </li>
        </ul>
    </div>
    ${body}
    <div class="layui-footer">
        <!-- 底部固定区域 -->
        © layui.com - 底部固定区域
    </div>
</div>
</body>
<script src="${ctx}/lib/layui/layui.js"></script>
<script>
    function iframe_onload() {
        
    }
    layui.use(['element', 'form', 'layedit', 'laydate','table'], function () {
        var $ = layui.jquery
                , element = layui.element; //Tab的切换功能，切换事件监听等，需要依赖element模块
        
        //触发事件
        var active = {
            tabAdd: function () {
                //新增一个Tab项
                element.tabAdd('demo', {
                    title: '新选项' + (Math.random() * 1000 | 0) //用于演示
                    , content: '内容' + (Math.random() * 1000 | 0)
                    , id: new Date().getTime() //实际使用一般是规定好的id，这里以时间戳模拟下
                })
            }
        };

    });
</script>
</html>
</#macro>