<#include "/layout/system.ftl">
<@head>
<title>home</title>
<link href="${ctx}/css/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
<script src="${ctx}/js/bootstrap/bootstrap-table/bootstrap-table.min.js"></script>
<script src="${ctx}/js/bootstrap/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
<script src="${ctx}/js/core/lookup.js"></script>
</@head>
<@body>
<form action="${ctx}/demo/imp" method="post" enctype="multipart/form-data">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <input type="file" name="importFile">
    <input type="submit" value="tijiao">
</form>
<input type="button" id="lookup" value="lookup">
<script>
    $('#model').lookup({
        ctx:"${ctx}",
        single:false,
        code:"system_user_list",
        callback:function (data) {
            alert(data[0].name);
        }
    })
</script>
</@body>