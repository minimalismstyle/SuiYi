<#include "/layout/sys.ftl">
<@head>
<title>home</title>
<link href="${ctx}/css/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
<script src="${ctx}/js/bootstrap/bootstrap-table/bootstrap-table.min.js"></script>
<script src="${ctx}/js/bootstrap/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
</@head>
<@body>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-sm-12 animated fadeInRight">
            <div class="mail-box-header">

                <form method="post" action="index.html" class="pull-right mail-search">
                    <div class="input-group">
                        <input type="text" class="form-control input-sm" name="search" placeholder="搜索邮件标题，正文等">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-sm btn-primary">
                                搜索
                            </button>
                        </div>
                    </div>
                </form>
                <h2>
                    用户维护
                </h2>
                <div class="mail-tools m-t-md">
                    <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="left" title="刷新邮件列表"><i
                            class="fa fa-refresh"></i> 刷新
                    </button>
                    <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="标为重要"><i
                            class="fa fa-plus"></i>
                    </button>
                    <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="标为垃圾邮件"><i
                            class="fa fa-trash-o"></i>
                    </button>

                </div>
            </div>
            <div class="mail-box">
                <table data-toggle="table" data-url="${ctx}/system/user/findAll" striped="true"
                       data-query-params="queryParams" data-mobile-responsive="true" data-height="400"
                       data-pagination="true" data-icon-size="outline">
                    <thead>
                    <tr>
                        <th data-field="state" data-checkbox="true"></th>
                        <th data-field="id">ID</th>
                        <th data-field="name">名称</th>
                        <th data-field="price">价格</th>
                    </tr>
                    </thead>
                </table>


            </div>
        </div>
    </div>
</div>
<script>
</script>
</@body>