package com.woodws.sys.util;

import com.woodws.sys.entity.AccountInfo;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Created by maoxiaodong on 2016/10/25.
 */
public class SecurityUtil {

    public static AccountInfo getPrincipal() {
        AccountInfo account = null;
        if (SecurityContextHolder.getContext() != null && SecurityContextHolder.getContext().getAuthentication() != null) {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (principal != null && principal instanceof AccountInfo) {
                account = (AccountInfo) principal;
            }
        }
        return account;
    }
}
