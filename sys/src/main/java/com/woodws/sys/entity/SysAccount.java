package com.woodws.sys.entity;

import com.woodws.core.data.SnowflakeGenerator;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by maoxiaodong on 2016/10/14.
 */
@Entity
public class SysAccount implements Serializable {
    @Id
    @GenericGenerator(name = "snowflake", strategy = SnowflakeGenerator.TYPE)
    @GeneratedValue(generator = "snowflake")
    protected Long id;


    protected String username;


    protected String password;

    protected Long userId;

    @Transient
    private SysUser user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public SysUser getUser() {
        return user;
    }

    public void setUser(SysUser user) {
        this.user = user;
    }
}
