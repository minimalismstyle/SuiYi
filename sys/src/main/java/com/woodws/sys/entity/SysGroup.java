package com.woodws.sys.entity;

import com.woodws.core.data.SnowflakeGenerator;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

@Entity
public class SysGroup {

    @Id
    @GenericGenerator(name = "snowflake", strategy = SnowflakeGenerator.TYPE)
    @GeneratedValue(generator = "snowflake")
    private Long id;


    private String code;


    private String name;


    private String disabled;


    private String description;

    @Transient
    private List<SysGroup> childrenGroup;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisabled() {
        return disabled;
    }

    public void setDisabled(String disabled) {
        this.disabled = disabled;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<SysGroup> getChildrenGroup() {
        return childrenGroup;
    }

    public void setChildrenGroup(List<SysGroup> childrenGroup) {
        this.childrenGroup = childrenGroup;
    }
}
