package com.woodws.sys.entity;

import com.woodws.core.data.SnowflakeGenerator;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by maoxiaodong on 2016/10/26.
 */
@Entity
public class SysMenu implements Serializable{

    @Id
    @GenericGenerator(name = "snowflake", strategy = SnowflakeGenerator.TYPE)
    @GeneratedValue(generator = "snowflake")
    private Long id;


    private String name;


    private String url;


    private Integer type;


    private String show;


    private String disabled;


    private String icon;


    private String target;

    private String sort;


    private String parentId;

    @Transient
    private List<SysMenu> childrenMenus;

    @Transient
    private int childrenCount;

    public SysMenu() {
    }

    public SysMenu(Long id) {
        this.id = id;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getShow() {
        return show;
    }

    public void setShow(String show) {
        this.show = show;
    }

    public String getDisabled() {
        return disabled;
    }

    public void setDisabled(String disabled) {
        this.disabled = disabled;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public List<SysMenu> getChildrenMenus() {
        return childrenMenus;
    }

    public void setChildrenMenus(List<SysMenu> childrenMenus) {
        this.childrenMenus = childrenMenus;
    }

    public boolean hasChildren() {
        return getChildrenCount() > 0;
    }

    public int getChildrenCount() {

        if (childrenMenus == null) {
            return 0;
        } else {
            return childrenMenus.size();
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysMenu menu = (SysMenu) o;

        return id.equals(menu.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
