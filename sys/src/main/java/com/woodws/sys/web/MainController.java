package com.woodws.sys.web;

import com.slyak.spring.jpa.PageParams;
import com.woodws.sys.entity.SysUser;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by maoxiaodong on 2016/10/21.
 */
@Controller
@RequestMapping
public class MainController implements ErrorController {

    @RequestMapping(value = {"/", "home"}, method = RequestMethod.GET)
    public String home(HttpServletRequest request) {
        SecurityContext securityContext = (SecurityContext) request.getSession().getAttribute("SPRING_SECURITY_CONTEXT");
        securityContext.getAuthentication().getPrincipal();
        return "home";
    }

    @RequestMapping(value = "login", method = RequestMethod.GET)
    public String login(PageParams params) {
        return "login";
    }

    @RequestMapping(value = "logout", method = RequestMethod.GET)
    public String logout() {
        return "login";
    }


    @RequestMapping(value = "/error/404", method = RequestMethod.GET)
    public String error_404() {
        return "/error/404";
    }

    @RequestMapping(value = "/error/500", method = RequestMethod.GET)
    public String error_500() {
        return "/error/500";
    }

    @Override
    public String getErrorPath() {
        return "";
    }
}
