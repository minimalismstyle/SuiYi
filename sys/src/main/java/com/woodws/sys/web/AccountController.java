package com.woodws.sys.web;

import com.woodws.core.web.WebController;
import com.woodws.sys.entity.SysAccount;
import com.woodws.sys.service.AccountService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/system/account/")
public class AccountController extends WebController<AccountService, SysAccount, Long> {

}
