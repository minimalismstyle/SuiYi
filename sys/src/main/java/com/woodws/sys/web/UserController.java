package com.woodws.sys.web;

import com.woodws.core.web.WebController;
import com.woodws.sys.entity.SysUser;
import com.woodws.sys.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/system/user/")
public class UserController extends WebController<UserService, SysUser, Long> {

}
