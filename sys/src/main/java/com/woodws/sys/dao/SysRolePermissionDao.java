package com.woodws.sys.dao;

import com.woodws.sys.entity.SysRolePermission;
import com.woodws.core.dao.BaseDao;

import java.util.List;
import java.util.Set;

public interface SysRolePermissionDao extends BaseDao<SysRolePermission, Long> {

    Set<SysRolePermission> findByTypeAndRoleIdIn(String type,List roles);
}
