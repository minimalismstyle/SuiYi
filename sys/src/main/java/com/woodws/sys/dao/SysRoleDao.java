package com.woodws.sys.dao;

import com.woodws.sys.entity.SysRole;
import com.woodws.core.dao.BaseDao;

import java.util.List;


public interface SysRoleDao extends BaseDao<SysRole, Long> {
    List<SysRole> findByIdIn(List<Long> id);
}
