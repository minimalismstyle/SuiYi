package com.woodws.sys.dao;

import com.woodws.sys.entity.SysUserRole;
import com.woodws.core.dao.BaseDao;

import java.util.List;


public interface SysUserRoleDao extends BaseDao<SysUserRole, Long> {

    List<SysUserRole> findByUserId(Long userId);
}
