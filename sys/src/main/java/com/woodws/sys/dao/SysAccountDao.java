package com.woodws.sys.dao;

import com.woodws.sys.entity.SysAccount;
import com.woodws.core.dao.BaseDao;

/**
 * Created by maoxiaodong on 2016/10/14.
 */
public interface SysAccountDao extends BaseDao<SysAccount, Long> {
    SysAccount findByUsername(String username);
}
