package com.woodws.sys.dao;

import com.woodws.sys.entity.SysMenu;
import com.woodws.core.dao.BaseDao;

import java.util.List;

public interface SysMenuDao extends BaseDao<SysMenu, Long> {

    List<SysMenu> findByShowAndDisabledAndParentIdNull(String show,String disabled);

    List<SysMenu> findByShowAndDisabledAndParentId(String show,String disabled,Long parentId);
}
