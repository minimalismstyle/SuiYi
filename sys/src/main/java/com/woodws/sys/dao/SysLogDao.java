package com.woodws.sys.dao;

import com.slyak.spring.jpa.OV;
import com.slyak.spring.jpa.PageParams;
import com.slyak.spring.jpa.TemplateQuery;
import com.woodws.sys.entity.SysLog;
import com.woodws.core.dao.BaseDao;
import org.springframework.data.domain.Page;

import java.util.List;

public interface SysLogDao extends BaseDao<SysLog, Long> {

    @TemplateQuery
    Page<OV> fndMapByPageParams(PageParams params);

    @TemplateQuery
    List<SysLog> fndSysLogByPageParams(PageParams params);
}
