package com.woodws.sys.dao;

import com.woodws.sys.entity.SysGroup;
import com.woodws.core.dao.BaseDao;


public interface SysGroupDao extends BaseDao<SysGroup, Long> {


}
