package com.woodws.sys.dao;

import com.woodws.sys.entity.SysUser;
import com.woodws.core.dao.BaseDao;

public interface SysUserDao extends BaseDao<SysUser, Long> {


}
