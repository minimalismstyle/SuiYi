package com.woodws.sys.service.impl;

import com.woodws.core.service.impl.BaseServiceImpl;
import com.woodws.sys.dao.SysRoleDao;
import com.woodws.sys.entity.SysRole;
import com.woodws.sys.service.RoleService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by maoxiaodong on 2016/10/16.
 */
@Service
public class RoleServiceImpl extends BaseServiceImpl<SysRoleDao,SysRole, Long>
        implements RoleService {


    @Override
    public List<SysRole> findByIdIn(List<Long> ids) {
        return dao.findByIdIn(ids);
    }
}
