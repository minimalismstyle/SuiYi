package com.woodws.sys.service.impl;

import com.woodws.core.service.impl.BaseServiceImpl;
import com.woodws.sys.dao.SysUserRoleDao;
import com.woodws.sys.entity.SysUserRole;
import com.woodws.sys.service.UserRoleService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maoxiaodong on 2016/10/16.
 */
@Service
public class UserRoleServiceImpl extends BaseServiceImpl<SysUserRoleDao,SysUserRole, Long>
        implements UserRoleService {
    @Override
    public List<SysUserRole> findByUserId(Long userId) {
        return dao.findByUserId(userId);
    }

    @Override
    public List<Long> findRoleIdByUserId(Long userId) {
        List<SysUserRole> userRoles = findByUserId(userId);
        List<Long> list = new ArrayList<Long>();
        for(SysUserRole userRole:userRoles){
            list.add(userRole.getRoleId());
        }
        return list;
    }
}
