package com.woodws.sys.service.impl;

import com.woodws.core.service.impl.BaseServiceImpl;
import com.woodws.sys.dao.SysUserDao;
import com.woodws.sys.entity.SysUser;
import com.woodws.sys.service.UserService;
import org.springframework.stereotype.Service;

/**
 * Created by maoxiaodong on 2016/10/16.
 */
@Service
public class UserServiceImpl extends BaseServiceImpl<SysUserDao,SysUser, Long>
        implements UserService {


}
