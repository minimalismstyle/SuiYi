package com.woodws.sys.service;

import com.woodws.core.service.BaseService;
import com.woodws.sys.entity.SysUser;

/**
 * Created by maoxiaodong on 2016/10/16.
 */
public interface UserService extends BaseService<SysUser, Long> {
}
