package com.woodws.sys.service.impl;

import com.slyak.spring.jpa.OV;
import com.slyak.spring.jpa.PageParams;
import com.woodws.core.data.PageParamsImpl;
import com.woodws.core.service.impl.BaseServiceImpl;
import com.woodws.sys.dao.SysLogDao;
import com.woodws.sys.entity.SysLog;
import com.woodws.sys.service.SystemLogService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by maoxiaodong on 2016/10/16.
 */
@Service
public class SystemLogServiceImpl extends BaseServiceImpl<SysLogDao,SysLog, Long>
        implements SystemLogService {
    @Override
    public SysLog findByEndTimeIsNotNull() {
        PageParams params = new PageParamsImpl(0,5);
        Page<OV> logs1 = dao.fndMapByPageParams(params);
        //OV ov = logs1.getContent().get(0);
        //params.addParams(ov);
        //List<SysLog> logs2 = dao.fndSysLogByPageParams(params);
        return null;
    }
}
