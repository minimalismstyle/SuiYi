package com.woodws.sys.service;

import com.woodws.core.service.BaseService;
import com.woodws.sys.entity.SysMenu;
import com.woodws.sys.entity.SysRolePermission;

import java.util.List;

/**
 * Created by maoxiaodong on 2016/10/16.
 */
public interface RolePermissionService extends BaseService<SysRolePermission, Long> {
    List<SysMenu> findUserMenus(Long userId);

    List<SysMenu> findUserMainMenus(List<Long> roleIds);
    List<SysMenu> findUserMenus(List<Long> roleIds);
}
