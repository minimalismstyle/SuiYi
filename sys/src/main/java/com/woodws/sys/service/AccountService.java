package com.woodws.sys.service;

import com.woodws.core.service.BaseService;
import com.woodws.sys.entity.SysAccount;

/**
 * Created by maoxiaodong on 2016/10/21.
 */
public interface AccountService extends BaseService<SysAccount, Long> {
    SysAccount findByUsername(String username);
}
