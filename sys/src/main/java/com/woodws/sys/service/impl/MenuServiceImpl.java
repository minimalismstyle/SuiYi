package com.woodws.sys.service.impl;

import com.woodws.core.service.impl.BaseServiceImpl;
import com.woodws.sys.dao.SysMenuDao;
import com.woodws.sys.entity.SysMenu;
import com.woodws.sys.service.MenuService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by maoxiaodong on 2016/10/16.
 */
@Service
public class MenuServiceImpl extends BaseServiceImpl<SysMenuDao, SysMenu, Long>
        implements MenuService {

    @Override
    public List<SysMenu> findByMain() {
        return dao.findByShowAndDisabledAndParentId("Y", "N",0L);
    }

    @Override
    public List<SysMenu> findShowAll() {
        List<SysMenu> menus = dao.findByShowAndDisabledAndParentIdNull("Y", "N");
        setChildrenMenus(menus, 1, 3);
        return menus;
    }

    private void setChildrenMenus(List<SysMenu> menus, int level, int childrenLevel) {
        if (level > childrenLevel) {
            return;
        }
        for (SysMenu menu : menus) {
            List<SysMenu> childrenMenus = dao.findByShowAndDisabledAndParentId("Y", "N", menu.getId());
            menu.setChildrenMenus(childrenMenus);
            if (menu.hasChildren()) {
                setChildrenMenus(childrenMenus, level + 1,childrenLevel);
            }
        }
    }
}
