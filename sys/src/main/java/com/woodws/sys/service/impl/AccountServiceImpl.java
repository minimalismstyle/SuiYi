package com.woodws.sys.service.impl;

import com.woodws.core.service.impl.BaseServiceImpl;
import com.woodws.sys.dao.SysAccountDao;
import com.woodws.sys.entity.SysAccount;
import com.woodws.core.util.PasswordUtil;
import com.woodws.sys.service.AccountService;
import org.springframework.stereotype.Service;

/**
 * Created by maoxiaodong on 2016/10/21.
 */
@Service
public class AccountServiceImpl
        extends BaseServiceImpl<SysAccountDao, SysAccount, Long>
        implements AccountService {

    @Override
    public SysAccount save(SysAccount account) {
        String password = PasswordUtil.generateEncodedPassword(account.getPassword());
        account.setPassword(password);
        return super.save(account);
    }

    @Override
    public SysAccount findByUsername(String username) {
        return dao.findByUsername(username);
    }

}
