package com.woodws.sys.service;

import com.woodws.core.service.BaseService;
import com.woodws.sys.entity.SysUserRole;

import java.util.List;

/**
 * Created by maoxiaodong on 2016/10/16.
 */
public interface UserRoleService extends BaseService<SysUserRole, Long> {
    List<SysUserRole> findByUserId(Long userId);
    List<Long> findRoleIdByUserId(Long userId);
}
