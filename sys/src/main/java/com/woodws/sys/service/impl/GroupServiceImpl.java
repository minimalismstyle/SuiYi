package com.woodws.sys.service.impl;

import com.woodws.core.service.impl.BaseServiceImpl;
import com.woodws.sys.dao.SysGroupDao;
import com.woodws.sys.entity.SysGroup;
import com.woodws.sys.service.GroupService;
import org.springframework.stereotype.Service;

/**
 * Created by maoxiaodong on 2016/10/16.
 */
@Service
public class GroupServiceImpl extends BaseServiceImpl<SysGroupDao,SysGroup, Long>
        implements GroupService {
}
