package com.woodws.sys.service;

import com.woodws.core.service.BaseService;
import com.woodws.sys.entity.SysMenu;

import java.util.List;

/**
 * Created by maoxiaodong on 2016/10/16.
 */
public interface MenuService extends BaseService<SysMenu, Long> {
    List<SysMenu> findByMain();
    List<SysMenu> findShowAll();
}
