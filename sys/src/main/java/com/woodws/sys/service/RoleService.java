package com.woodws.sys.service;

import com.woodws.core.service.BaseService;
import com.woodws.sys.entity.SysRole;

import java.util.List;

/**
 * Created by maoxiaodong on 2016/10/16.
 */
public interface RoleService extends BaseService<SysRole, Long> {
    List<SysRole> findByIdIn(List<Long> id);
}
