<#assign form=JspTaglibs["http://www.springframework.org/tags/form"] />
<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentelella Alela! | 登录</title>
    <link href="${ctx}/lib/layui/css/layui.css" rel="stylesheet">
    <link href="${ctx}/lib/layui/css/global.css" rel="stylesheet">

</head>
<body class="login">
<div class="main layui-row">
    <div class="layui-form layui-tab-content" id="LAY_ucm" style="padding: 20px 0;">
        <div class="layui-tab-item layui-show">
            <div class="layui-form layui-form-pane">
            <@form.form  method="post">
                <div class="layui-form-item">
                    <label for="L_email" class="layui-form-label">邮箱</label>
                    <div class="layui-input-inline">
                        <input type="text" id="username" name="username" required lay-verify="required"
                               autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="L_pass" class="layui-form-label">密码</label>
                    <div class="layui-input-inline">
                        <input type="password" id="password" name="password" required lay-verify="required"
                               autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="L_vercode" class="layui-form-label">人类验证</label>
                    <div class="layui-input-inline">
                        <input type="text" id="L_vercode" name="vercode" required lay-verify="required"
                               placeholder="请回答后面的问题" autocomplete="off" class="layui-input">
                    </div>
                    <div class="layui-form-mid">
                        <span style="color: #c00;">1+1=?</span>
                    </div>
                </div>
                <div class="layui-form-item">
                    <button class="layui-btn" lay-filter="*" lay-submit>立即登录</button>
                    <span style="padding-left:20px;">
                  <a href="forget.html">忘记密码？</a>
                </span>
                </div>
                <div class="layui-form-item fly-form-app">
                    <span>或者使用社交账号登入</span>
                    <a href="http://fly.layui.com:8098/app/qq"
                       onclick="layer.msg('正在通过QQ登入', {icon:16, shade: 0.1, time:0})" class="iconfont icon-qq"
                       title="QQ登入"></a>
                    <a href="http://fly.layui.com:8098/app/weibo/"
                       onclick="layer.msg('正在通过微博登入', {icon:16, shade: 0.1, time:0})" class="iconfont icon-weibo"
                       title="微博登入"></a>
                </div>
            </@form.form>
            </div>
        </div>
    </div>
</div>
</body>
<script src="${ctx}/lib/jquery/jquery.min.js"></script>

<script>
    $(".submit").click(function () {
        $("form").submit();
    });
</script>
</html>
