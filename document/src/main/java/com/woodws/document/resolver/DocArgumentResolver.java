package com.woodws.document.resolver;

import com.woodws.document.annotation.DocAttribute;
import com.woodws.document.annotation.Import;
import com.woodws.document.interceptor.ImpInterceptor;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

public class DocArgumentResolver implements HandlerMethodArgumentResolver {

    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterAnnotation(DocAttribute.class) != null
                && parameter.getMethod().isAnnotationPresent(Import.class);
    }

    public Object resolveArgument(MethodParameter parameter,
                                  ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest,
                                  WebDataBinderFactory binderFactory) throws Exception {
        return ImpInterceptor.getParam(parameter.getParameterName());
    }

}
