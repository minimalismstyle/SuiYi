package com.woodws.document.interceptor;

import org.jxls.common.Context;
import org.jxls.util.JxlsHelper;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

/**
 * Created by maoxiaodong on 2016/10/27.
 */
public class ExpInterceptor extends HandlerInterceptorAdapter {

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object o, ModelAndView modelAndView) throws Exception {
        String templates = modelAndView.getViewName();
        Map<String, Object> map = modelAndView.getModel();
        InputStream is = ExpInterceptor.class.getResourceAsStream(templates);
        OutputStream os = response.getOutputStream();
        Context context = new Context(map);
        JxlsHelper.getInstance().processTemplate(is, os, context);
    }
}