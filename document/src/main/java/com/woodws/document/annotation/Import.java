package com.woodws.document.annotation;

import java.lang.annotation.*;

/**
 * Created by maoxiaodong on 2016/10/28.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Import {
    String value();

    String attr() default "importFile";
}
