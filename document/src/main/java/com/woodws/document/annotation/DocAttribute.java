package com.woodws.document.annotation;

import org.springframework.web.bind.annotation.SessionAttributes;

import java.lang.annotation.*;

/**
 * Created by maoxiaodong on 2016/11/2.
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DocAttribute {

}
