package com.woodws.document.entity;

import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * Created by maoxiaodong on 2016/10/28.
 */
public class ImpRepository {
    private Long id;
    private MultipartFile file;

    private Map<String, Long> params;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public Map<String, Long> getParams() {
        return params;
    }

    public void setParams(Map<String, Long> params) {
        this.params = params;
    }
}
