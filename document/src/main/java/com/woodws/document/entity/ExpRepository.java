package com.woodws.document.entity;

/**
 * Created by maoxiaodong on 2016/10/28.
 */
public class ExpRepository {
    private String type;
    private String name;
    private String template;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }
}
