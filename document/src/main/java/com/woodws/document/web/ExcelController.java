package com.woodws.document.web;

import com.woodws.document.entity.ExpRepository;
import com.woodws.util.DBUtil;
import org.jxls.common.Context;
import org.jxls.jdbc.JdbcHelper;
import org.jxls.util.JxlsHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;

/**
 * Created by maoxiaodong on 2016/10/28.
 */
@Controller
@RequestMapping("/document/excel/")
public class ExcelController {

    @Autowired
    private ApplicationContext applicationContext;

    @RequestMapping("/document/excel/exp")
    public void exp(ExpRepository dao, HttpServletResponse response) throws Exception {
        Connection conn = DBUtil.getConnection(applicationContext);
        JdbcHelper jdbcHelper=new JdbcHelper(conn) ;
        InputStream is = ExcelController.class.getResourceAsStream(dao.getTemplate());
        OutputStream os = response.getOutputStream();
        Context context = new Context();
        context.putVar("conn", conn);
        context.putVar("jdbc", jdbcHelper);
        JxlsHelper.getInstance().processTemplate(is, os, context);

    }
}
