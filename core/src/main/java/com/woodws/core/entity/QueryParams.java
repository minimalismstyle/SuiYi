package com.woodws.core.entity;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by maoxiaodong on 2016/11/2.
 */
public class QueryParams extends PageRequest {



    private String searchKey;

    private int page;
    private int size;

    private boolean isPage = false;

    private Map<String, String> params;

    public QueryParams(int page, int size) {
        super(page, size);
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public String get(String name) {
        if (params == null) {
            return null;
        }
        return params.get(name);
    }

    public void set(String name, String value) {
        if (params == null) {
            params = new HashMap<String, String>();
        }
        params.put(name, value);
    }

    public boolean isPage() {
        return isPage;
    }

    public String getSearchKey() {
        return searchKey;
    }

    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
        this.isPage = true;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
