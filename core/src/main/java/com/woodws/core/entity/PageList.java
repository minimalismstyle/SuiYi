package com.woodws.core.entity;

import org.springframework.data.domain.Page;

import java.util.List;

/**
 * Created by maoxiaodong on 2016/11/2.
 */
public interface PageList<E> extends List<E>{
    int getTotalPages();

    boolean isFirst();

    boolean isLast();

    boolean hasNext();

    boolean hasPrevious();

    int getPage();

    int getSize();
}
