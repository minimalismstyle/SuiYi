package com.woodws.core.entity;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by maoxiaodong on 2016/11/2.
 */
public class PageArrayList<E> extends ArrayList<E> implements PageList<E> {
    private int page;

    private int size;

    private int totalPages;

    public PageArrayList(int page,int totalPages, Collection<? extends E> c) {
        this.addAll(c);
        this.page = page;
        this.size = c.size();
        this.totalPages = totalPages;
    }

    @Override
    public int getTotalPages() {
        return totalPages;
    }

    @Override
    public boolean isFirst() {
        return page == 1;
    }

    @Override
    public boolean isLast() {
        return page == totalPages;
    }

    public boolean hasNext() {
        return page < totalPages;
    }

    public boolean hasPrevious() {
        return page != totalPages;
    }

    @Override
    public int getPage() {
        return page;
    }

    @Override
    public int getSize() {
        return size;
    }
}
