package com.woodws.core.annotation;

import java.lang.annotation.*;

/**
 * Created by maoxiaodong on 2016/10/31.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface I18n {
    String[] value();
}
