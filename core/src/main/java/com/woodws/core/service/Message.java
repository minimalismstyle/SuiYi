package com.woodws.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**
 * Created by maoxiaodong on 2016/10/31.
 */

@Component
public class Message {

    @Autowired
    private MessageSource messageSource;

    public String get(String code, Object... objects) {
        Locale locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage(code, objects, code, locale);
    }
}
