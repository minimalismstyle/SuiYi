package com.woodws.core.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.woodws.core.dao.BaseDao;
import com.woodws.core.entity.PageArrayList;
import com.woodws.core.entity.PageList;
import com.woodws.core.entity.QueryParams;
import com.woodws.core.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

/**
 * Created by maoxiaodong on 2016/10/16.
 */
public class BaseServiceImpl<D extends BaseDao<T, ID>, T, ID
        extends Serializable> implements BaseService<T, ID> {

    @Autowired
    protected D dao;

    @Transactional
    @Override
    public <S extends T> S  save(S entity) {
        return dao.save(entity);
    }

    @Transactional
    @Override
    public void delete(ID id) {
        dao.deleteById(id);
    }

    @Transactional
    @Override
    public void deleteAll() {
        dao.deleteAll();
    }

    @Override
    public T find(ID id) {
        return dao.findById(id).get();
    }

    @Override
    public List<T> findAll() {
        return dao.findAll();
    }

    @Override
    public List<T> findByParamsPage(QueryParams params) {
        Page page = PageHelper.startPage(params.getPage(), params.getSize());
        List<T> list = dao.findAll();
        PageList<T> pageList = new PageArrayList<T>(params.getPage(), (int) page.getTotal(),list);
        return pageList;
    }

    @Override
    public List<T> findByParams(QueryParams params) {
        return dao.findAll();
    }

    @Override
    public List<T> findBy(T entity) {
        return dao.findAll();
    }


    @Override
    public long count() {
        return dao.count();
    }

}
