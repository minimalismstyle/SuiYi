package com.woodws.core.service;

import com.woodws.core.entity.PageList;
import com.woodws.core.entity.QueryParams;

import java.io.Serializable;
import java.util.List;

/**
 * Created by maoxiaodong on 2016/10/16.
 */
public interface BaseService<T,ID extends Serializable> {
    <S extends T> S  save(S entity);

    void delete(ID id);

    void deleteAll();

    T find(ID id);

    List<T> findAll();

    List<T> findByParamsPage(QueryParams params);

    List<T> findByParams(QueryParams params);

    List<T> findBy(T entity);

    long count();

}
