package com.woodws.core.web;

import com.woodws.core.entity.QueryParams;
import com.woodws.core.service.BaseService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by maoxiaodong on 2016/10/21.
 */
public class WebController<S extends BaseService<E, ID>, E, ID extends Serializable> extends BaseController<S> {

    @RequestMapping
    public ModelAndView list() {
        return new ModelAndView(RESOURCE + "list");
    }

    @RequestMapping("find/{id}")
    @ResponseBody
    public E find(@PathVariable("id") ID id) {
        return service.find(id);
    }

    @RequestMapping("findAll")
    @ResponseBody
    public List<E> findAll() {
        return service.findAll();
    }

    @RequestMapping("findBy")
    @ResponseBody
    public List<E> findBy(E entity) {
        return service.findBy(entity);
    }

    @RequestMapping("findByParamsPage")
    @ResponseBody
    public List<E> findByParamsPage(QueryParams params) {
        return service.findByParamsPage(params);
    }


    @RequestMapping("findByParams")
    @ResponseBody
    public List<E> findByParams(QueryParams params) {
        return service.findByParams(params);
    }

    @RequestMapping("save")
    @ResponseBody
    public E save(E entity) {
        return service.save(entity);
    }

    @RequestMapping("delete/{id}")
    @ResponseBody
    public boolean delete(@PathVariable("id") ID id) {
        service.delete(id);
        return true;
    }

    @RequestMapping("add")
    public ModelAndView add() {
        return new ModelAndView(RESOURCE + "new");
    }

    @RequestMapping("form/{id}")
    public ModelAndView form(@PathVariable("id") ID id) {
        E entity = service.find(id);
        return new ModelAndView(RESOURCE + "form", "entity", entity);
    }

    @RequestMapping("view/{id}")
    public ModelAndView view(@PathVariable("id") ID id) {
        E entity = service.find(id);
        return new ModelAndView(RESOURCE + "view", "entity", entity);
    }

    @RequestMapping("list")
    public ModelAndView list(Map<String, Long> params) {
        List<E> list = service.findAll();
        return new ModelAndView(RESOURCE + "list", "list", list);
    }
}
