package com.woodws.core.web;

import com.woodws.util.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;
/**
 * Created by maoxiaodong on 2016/10/21.
 */
public class BaseController<S> {

    protected final String RESOURCE = WebUtil.getResource(this.getClass());

    @Autowired
    protected S service;
}
