package com.woodws.core.data;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

import java.io.Serializable;

public class SnowflakeGenerator implements IdentifierGenerator {
    public static final String TYPE = "com.woodws.core.data.SnowflakeGenerator";

    private static final IdWorker idWorker = new IdWorker();


    @Override
    public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
        return idWorker.getId();
    }
}
