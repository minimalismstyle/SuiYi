package com.woodws.util;

import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by maoxiaodong on 2016/10/30.
 */
public class WebUtil {
    public static Map<String, String> getParameters(HttpServletRequest request){
        Map<String, String> parameters = new HashMap<String, String>();
        Enumeration<String> names = request.getParameterNames();
        while (names.hasMoreElements()){
            String name = names.nextElement();
            parameters.put(name,request.getParameter(name));
        }
        return parameters;
    }

    public static String getResource(Class c){
        RequestMapping mapping = (RequestMapping) c.getAnnotation(RequestMapping.class);
        return mapping.value()[0];
    }
}
