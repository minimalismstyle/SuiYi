package com.woodws.util;

/**
 * Created by maoxiaodong on 2016/11/3.
 */
public class PathUtil {
    public static String join(String... paths) {
        String newPath = "";
        for (String path : paths) {
            if (!newPath.endsWith("/")) {
                newPath += "/";
            }

            if (path.startsWith("/")) {
                path = path.replace("/", "");
            }
            newPath += path;
        }
        return newPath;
    }
}
