package com.woodws.util;

import org.springframework.context.ApplicationContext;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by maoxiaodong on 2016/10/28.
 */
public class DBUtil {

    public static Connection getConnection(ApplicationContext applicationContext){
        DataSource dataSource = applicationContext.getBean(DataSource.class);
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return conn;
    }
}
