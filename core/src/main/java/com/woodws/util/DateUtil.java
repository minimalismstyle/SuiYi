package com.woodws.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Jenson on 2016-01-20.
 */
public class DateUtil {

    /**
     * 格式化日期
     *
     * @param date      源日期
     * @param formatter 格式划字符串，为空则采用默认值“yyyy-MM-dd”
     * @return 格式化后的字符串
     */
    public static String format(Date date, String formatter) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(formatter);
        return dateFormat.format(date);
    }

    /**
     * 格式化日期
     *
     * @param date      源日期
     * @return 格式化后的字符串 默认值“yyyy-MM-dd”
     */
    public static String format(Date date) {
        return format(date,"yyyy-MM-dd");
    }

    /**
     * 日期加减
     *
     * @param date  源日期
     * @param field 加减单位（年月日等）
     * @param mount 加减值
     * @return 运算后的日期
     */
    public static Date add(Date date, int field, int mount) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(field, mount);
        return calendar.getTime();
    }

    /**
     * 日期加减
     *
     * @param date  源日期
     * @param days 加减值
     * @return 运算后的日期
     */
    public static Date add(Date date, int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(5, days);
        return calendar.getTime();
    }

    /**
     * 根据字符串及格式字符串获取日期
     * @param dateSting 日期字符串
     * @param formatter 格式化字符串
     * @return 格式化后的日期
     * @throws ParseException
     */
    public static Date get(String dateSting, String formatter){
        SimpleDateFormat dateFormat = new SimpleDateFormat(formatter);
        try {
            return dateFormat.parse(dateSting);
        } catch (ParseException e) {
            throw new RuntimeException();
        }
    }

    /**
     * 根据字符串及格式字符串获取日期
     * @param dateSting 日期字符串
     * @return 格式化后的日期
     * @throws ParseException
     */
    public static Date get(String dateSting){
        return get(dateSting,"yyyy-MM-dd");
    }
}
