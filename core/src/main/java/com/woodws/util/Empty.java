package com.woodws.util;

import java.util.List;

public class Empty {
	public static boolean is(String target){
		if(target == null || target.equals("")){
			return true;
		}else{
			return false;
		}
	}
	
	public static boolean is(StringBuffer target){
		if(target == null || target.length() == 0){
			return true;
		}else{
			return false;
		}
	}
	
	public static boolean is(List target){
		if(target == null || target.isEmpty()){
			return true;
		}else{
			return false;
		}
	}
	
	public static boolean is(Object target){
		if(target == null){
			return true;
		}else{
			return false;
		}
	}
	
}
